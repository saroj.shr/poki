#! /bin/sh
IMAGE_NAME=$CI_REGISTRY/poki:$CI_PIPELINE_ID
curl -L https://github.com/wagoodman/dive/archive/v0.9.2.tar.gz > dive.tar.gz
tar -xvf dive.tar.gz
podman login -u gitlab-ci-token -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
podman pull $IMAGE_NAME
./dive podman://$IMAGE_NAME