# #!/bin/sh
# IMAGE_NAME=$CI_REGISTRY/poki:$CI_PIPELINE_ID

# echo $IMAGE_NAME
# echo $CI_REGISTRY_PASSWORD
# echo buildah --version

# # buildah login -u gitlab-ci-token -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
# buildah login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
# buildah bud -t $IMAGE_NAME .
# echo "Done with the build!"
# buildah push --authfile ${XDG_RUNTIME_DIR}/containers/auth.json $IMAGE_NAME


#!/bin/sh
IMAGE_NAME=$CI_REGISTRY/poki:$CI_PIPELINE_ID

podman login -u $CI_DEPLOY_USER -p $CI_DEPLOY_PASSWORD $CI_REGISTRY
podman build --tag $IMAGE_NAME .
echo "Done with the build!"
podman push $IMAGE_NAME