#!/bin/sh
IMAGE_NAME=$CI_REGISTRY/poki

skopeo login -u gitlab-ci-token -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
skopeo copy docker://$IMAGE_NAME:$CI_PIPELINE_ID docker://$IMAGE_NAME:latest