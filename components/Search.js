import React from 'react';
import { Container, makeStyles, TextField } from '@material-ui/core';

const useStyle = makeStyles((theme) => ({
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },

  // search: {
  //   position: 'relative',
  //   paddingTop: '10',
  //   paddingBottom: '20',
  //   width: '100%'
  // },
}));

export default function Search() {
  const classes = useStyle();

  return (
    <Container className={classes.root}>
      <TextField
        label="Search Pokemon"
        id="standard-size-normal"
        autoFocus
        fullWidth
      />
    </Container>
  );
}
