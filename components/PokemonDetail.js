import React, { useState } from 'react';
import useSWR, { SWRConfig } from 'swr';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { LinearProgress } from '@material-ui/core';

import PokemonBaseStat from './PokemonBaseStat';
import PokemonMoves from './PokemonMoves';

const fetcher = (...args) => fetch(...args).then((res) => res.json());

export default function PokemonDetail(pokemon) {
  const [pokemonImage, setPokemonImage] = useState(1);

  const pokemonSpecies = pokemon.pokemon.pokemon_species;
  const { data, error } = useSWR(
    `https://pokeapi.co/api/v2/pokemon/${pokemon.pokemon.pokemon_species.name}`,
    fetcher
  );

  const useStyles = makeStyles((theme) => ({
    root: {
      height: '100vh',
    },
    image: {
      backgroundImage: `url(https://pokeres.bastionbot.org/images/pokemon/${pokemon.pokemon.entry_number}.png)`,
      backgroundRepeat: 'no-repeat',
      backgroundColor:
        theme.palette.type === 'light'
          ? theme.palette.grey[50]
          : theme.palette.grey[900],
      backgroundSize: 'contain',
      backgroundPosition: 'center',
    },
    paper: {
      margin: theme.spacing(8, 4),
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    avatar: {
      margin: theme.spacing(1),
      backgroundColor: theme.palette.secondary.main,
    },
    form: {
      width: '100%', // Fix IE 11 issue.
      marginTop: theme.spacing(1),
    },
    submit: {
      margin: theme.spacing(3, 0, 2),
    },
  }));

  const classes = useStyles();

  return (
    <>
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          {error ? <p>error</p> : ''}
          {!data ? <LinearProgress color="secondary" /> : ''}
          {data ? (
            <Box>
              <PokemonBaseStat data={data} />
              <PokemonMoves data={data} />
            </Box>
          ) : (
            ''
          )}
        </Grid>
      </Grid>
    </>
  );
}
